﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace First
{
    class Program
    {
        private static List<User> FillUsers()
        {
            List<User> users = new List<User>
            {
                new User{Name = "Tom", Company = "Microsoft"},                
                new User{Name = "Tommy", Company = "Amazon"}, 
                new User{Name = "Tommas", Company = "Tesla"},
                new User{Name = "Tony", Company = "BurgerKing"},
                new User{Name = "Anton", Company = "KFC"},
                new User{Name = "Ivan", Company = "Ford"},
                new User{Name = "Joe", Company = "Foxtrot"},
                new User{Name = "Rogan", Company = "Amazon"},
                new User{Name = "Louis", Company = "KFC"},
                new User{Name = "Bernard", Company = "Ford"},
                new User{Name = "Ringo", Company = "KFC"},
                new User{Name = "Pulke", Company = "Microsoft"},
                new User{Name = "Donny", Company = "KFC"},
                new User{Name = "Paull", Company = "Tesla"},
                new User{Name = "Reggie", Company = "Foxtrot"},
                new User{Name = "Rich", Company = "Microsoft"}

            };
            return users;
        }

        static void Main(string[] args)
        {
            var users = FillUsers();
                        
            var companies = (from u in users
                            select u.Company).Distinct();

            // List<User> ou = users.GroupBy(u => u.Company).Select(b => b.FirstOrDefault()).ToList();
            // What is the best?
            // I think we have to save the solution above
                       
            var ou = users.DistinctBy(u => u.Company);

            foreach(var i in ou)
                Console.WriteLine(i.Name);
        }
    }
}
